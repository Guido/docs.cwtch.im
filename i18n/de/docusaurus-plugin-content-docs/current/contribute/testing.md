---
sidebar_position: 1
---

# Cwtch testen

Dieser Abschnitt dokumentiert einige Möglichkeiten, um mit Cwtch-Tests zu beginnen.

### Laufender Fuzzbot

FuzzBot ist unser Entwicklungs-Testbot. Du kannst FuzzBot als Kontakt hinzufügen: `cwtch:4y2hxlxqzautabituedksnh2ulcgm2coqbure6wvfpg4gi2ci25ta5ad`.

:::info FuzzBot Hilfe

Wenn Du FuzzBot eine `Hilfe-` Nachricht sendest, werden alle derzeit verfügbaren Testbefehle angezeigt.

:::

Weitere Informationen über FuzzBot findest Du in unserem [Discreet Log Development Blog](https://openprivacy.ca/discreet-log/07-fuzzbot/).

### Trete der Cwtch Release Candidate Tester Gruppe bei

Wenn Du Fuzzbot den Befehl `testgroup-invite` sendest, lädt FuzzBot Dich zur **Cwtch Testers Group** ein! Dort kannst du Fragen stellen, Fehlerberichte schreiben und Feedback geben.

### Cwtch Nightlies

Cwtch Nightly Builds sind Entwicklungs-Builds, die neue Features enthalten, die zum Testen bereit sind.

Die aktuellsten Entwicklungsversionen von Cwtch sind auf unserem [Build-Server](https://build.openprivacy.ca/files/) verfügbar.

Wir empfehlen **nicht**, dass Tester immer auf die neueste Nightly aktualisieren, sondern wir werden eine Nachricht an die Cwtch Release Candidate Testers Gruppe schicken, wenn eine bedeutsame Nightly verfügbar wird. Eine Nightly wird als bedeutsam angesehen, wenn sie eine neue Funktion oder eine größere Fehlerbehebung enthält.
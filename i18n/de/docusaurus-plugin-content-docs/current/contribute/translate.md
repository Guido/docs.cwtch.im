---
sidebar_position: 2
---

# Cwtch übersetzen

Wenn du Übersetzungen zu Cwtch beitragen möchtest, entweder zur App oder zu diesem Handbuch, kommt hier wie dies möglich ist.

## Cwtch-Anwendung

Die Anwendung wird über [Lokalise](https://lokalise.com) übersetzt.
1. Registriere dich für ein Konto auf der Lokalise Website
2. Sende eine E-Mail an [team@cwtch.im](mailto:team@cwtch.im) mit der Information, dass du gerne bei der Übersetzung mithelfen möchtest und teile die E-Mail-Adresse mit, mit der du dich angemeldet hast. Wir werden dich dann zum Projekt hinzufügen.

## Cwtch-Benutzerhandbuch

Das Handbuch wird über [Crowdin](https://crowdin.com) übersetzt.
1. Registriere dich für ein Konto auf der Crowdin Website.
2. Gehe zum [cwtch-users-handbuch](https://crowdin.com/project/cwtch-users-handbook) Projekt und trete bei.
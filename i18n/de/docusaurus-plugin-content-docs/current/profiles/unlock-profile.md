---
sidebar_position: 5
---

# Verschlüsselte Profile entsperren

Wenn du Cwtch neu startest und ein [Passwort](/docs/profiles/change-password/) in deinem Profil benutzst, dann wird es nicht als Standard geladen, sondern Du musst es erst entsperren.

1. Drücke das rosa Entsperr-Symbol <img src="/img/lock_open-24px.webp" className="ui-button" alt="Entsperr-Symbol" />
2. Kennwort eingeben
3. Klicke auf "Profil entsperren"

Siehe auch: [Cwtch Sicherheits-Handbuch: Profil Verschlüsselung & Speicher](https://docs.openprivacy.ca/cwtch-security-handbook/profile_encryption_and_storage.html)

---
sidebar_position: 11
---

# Profil importieren

1. Drücke die `+` Aktionstaste in der rechten unteren Ecke und wähle "Importiere Profil"
2. Wähle eine [exportierte Cwtch-Profildatei](/docs/profiles/exporting-profile) zum Importieren
3. Gib das [Passwort](/docs/profiles/create-a-profile#a-note-on-password-protected-encrypted-profiles) ein, das dem Profil zugeordnet ist und bestätige es.

Nach der Bestätigung wird Cwtch versuchen, die angegebene Datei mit einem aus dem angegebenen Passwort abgeleiteten Schlüssel zu entschlüsseln. Wenn es erfolgreich ist, erscheint das Profil auf dem Profilverwaltungs-Seite und kann verwendet werden.

:::note Hinweis

Während ein Profil auf mehrere Geräte importiert werden kann zur Zeit nur eine Version des Profils aktiv sein. Ein gleichzeitiger Betrieb auf allen Geräten ist nicht möglich.

Versuche, das gleiche Profil auf mehreren Geräten gleichzeitig zu verwenden, können zu Verfügbarkeitsproblemen und Nachrichtenfehlern führen.

:::
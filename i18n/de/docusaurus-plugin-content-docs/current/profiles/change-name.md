---
sidebar_position: 2
---

# Ändern Deines Anzeigenamens

1. In der Ansicht Profile verwalten, klicke auf den Stift neben dem Profil, das Du bearbeiten möchtest
2. Ändere deinen Namen
3. Profil speichern klicken
---
sidebar_position: 1.5
---

# Ein neues Profil erstellen

1. Drücke die `+` Aktionstaste in der rechten unteren Ecke und wähle "Neues Profil"
2. Wähle einen Anzeigenamen
3. Wähle dies aus, wenn Du dieses Profil lokal mit starker Verschlüsselung schützen möchtest:
    - Passwort: Dein Konto ist vor anderen Personen geschützt, die dieses Gerät evtl. verwenden können
    - Kein Passwort: Jeder, der Zugriff auf dieses Gerät hat, kann auf dieses Profil zugreifen
4. Gib dein Passwort ein und gib es erneut ein
5. Neues Profil hinzufügen anklicken

## Eine Anmerkung zu passwortgeschützten (verschlüsselten) Profilen

Die Profile werden lokal auf der Festplatte gespeichert und mittels eines Schlüssels verschlüsselt, der von einem Benutzer bekannten Passwort abgeleitet wird (via pbkdf2).

Beachte, dass einmal verschlüsselt und auf der Festplatte gespeichert die einzige Möglichkeit, ein Profil wiederherzustellen, ist das erneute Ableiten des Passworts - so ist es nicht möglich, eine vollständige Liste der Profile anzugeben, auf die ein Benutzer Zugriff haben könnte, bis er ein Passwort eingegeben hat.

Siehe auch: [Cwtch Sicherheits-Handbuch: Profil Verschlüsselung & Speicher](https://docs.openprivacy.ca/cwtch-security-handbook/profile_encryption_and_storage.html)

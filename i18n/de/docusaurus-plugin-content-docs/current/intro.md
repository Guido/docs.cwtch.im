---
sidebar_position: 1
---

# Was ist Cwtch?

Cwtch (/kʊtʃ/ - ein walisisches Wort, das grob übersetzt „eine Umarmung, die einen sicheren Ort schafft“ bedeutet) ist eine dezentralisierte, Privatspähre bewahrende, metadatenresistente Messenger-App.

* **Dezentralisiert und offen**: Es gibt keinen „Cwtch-Dienst“ oder „Cwtch-Netzwerk“. Die Cwtch Teilnehmer können ihre eigenen sicheren Räume hosten oder ihre Infrastruktur anderen anbieten, die auf der Suche nach einem sicheren Raum sind. Das Cwtch-Protokoll ist offen und jeder kann Bots, Dienste und Benutzeroberflächen erstellen und in Cwtch integrieren und interagieren.
* **Datenschutz wahrend**: Alle Kommunikation in Cwtch ist Ende-zu-Ende verschlüsselt und findet über Tor v3 onion Dienste statt.
* **Metadaten resistent**: Cwtch wurde so konzipiert, dass ohne ihre ausdrückliche Zustimmung keine Informationen ausgetauscht oder zugänglich sind einschließlich On-the-wire Nachrichten und Protokoll-Metadaten.


# Sicherheit und Verschlüsselung

Für einen detaillierteren Blick auf die in Cwtch verwendete Sicherheit, Privatsphäre und die zugrunde liegende Verschlüsselungstechnologie lesen Sie ies bitte unser [Sicherheitshandbuch](https://docs.openprivacy.ca/cwtch-security-handbook/)

# Erste Schritte

Du kannst die neueste Version von Cwtch von [herunterladen https://cwtch.im/download/](https://cwtch.im/download/)


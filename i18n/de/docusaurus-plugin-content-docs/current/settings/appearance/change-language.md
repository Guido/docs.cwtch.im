---
sidebar_position: 1
---

# Sprache wechseln

[Dank der Hilfe von Freiwilligen](https://docs.cwtch.im/docs/contribute/translate)wurde die Cwtch-App in viele Sprachen übersetzt.

Um die in Cwtch verwendete Sprache zu ändern:

1. Einstellungen öffnen
2. Die oberste Einstellung ist "Sprache", Du kannst das Drop-Down-Menü benutzen, um die Sprache auszuwählen, die Du verwenden möchtest.
---
sidebar_position: 5
---

# Experiment klickbarer Links

:::danger Gefahr

Wenn aktiviert, stellt diese Funktion ein **Entanonymisierung** Risiko dar.

**keine** URLs von Personen öffnen, denen Du nicht vertraust. Links, die über Cwtch gesendet werden, werden über den **Standard** Browser auf dem System geöffnet. Die meisten Webbrowser können keine Anonymität anbieten.

:::

# Aktiviere das Experiment mit klickbaren Links

Anklickbare Links sind standardmäßig **nicht** aktiviert. Um Cwtch das Öffnen von Links in Nachrichten zu ermöglichen:

1. Zu den Einstellungen
2. Experimente aktivieren
3. Aktiviere das `Klickbare Links` Experiment

![](/img/clickable_links_experiment.png)

## Risiko

Klickbare Links in Nachrichten sind ein sehr nützliches Feature, aber es gibt Risiken, die Du beachten solltest, wenn Du diese Funktion aktivierst.

Um eine versehentliche Auslösung zu verhindern, öffnet Cwtch nach einem Klick auf einen Link in einer Nachricht zuerst eine zusätzliche Aufforderung mit zwei Optionen:

![](/img/clickable_links.png)

1. Die URL in die Zwischenablage kopieren
2. Öffne die URL im **-Standard-Browser**

Du kannst die Zurück-Taste auf DeinemGerät verwenden oder von dieser Eingabeaufforderung wegklicken, um die Auswahl einer der beiden Optionen zu vermeiden.

**Cwtch kann Dich nicht schützen, wenn Du böswillige Links öffnest**.

Die URL wird im **-Standard-Browser** geöffnet, was wahrscheinlich mindestens Deine IP-Adresse dem Server bekanntgeben wird, der die URL hostet. Webseiten können auch andere Browser-Verwundbarkeiten nutzen, um zusätzliche Informationen zu sammeln oder Deinen Computer weiter auszunutzen.

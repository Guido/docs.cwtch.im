---
sidebar_position: 1
---

# Eine Einführung in die Cwtch App-Einstellungen

## Darstellung

Dies sind Einstellungen, die das Aussehen von Cwtch beeinflussen, einschließlich Themen und Lokalisierung.

## Verhalten

Diese Einstellungen beeinflussen die Reaktion von Cwtch auf bestimmte Ereignisse wie z.B. Benachrichtigungen für neue Nachrichten oder Anfragen von unbekannten öffentlichen Adressen.

## Experimente

Es gibt viele Funktionen in Cwtch, die man gerne ausprobieren möchte, deren Implementierung aber zusätzliche Metadaten erfordert oder Risiko, über das Minimum hinaus, das Cwtch für grundlegende Operationen benötigt, hinausgeht.

Unter **Experimente** findest du eine Anzahl von **optionalen** Einstellungen, die, wenn aktiviert, Dir zusätzliche Funktionen wie Gruppenchat, Dateiaustausch oder Nachrichtenformatierung bieten.

Du solltest sorgfältig über die neuen Risiken nachdenken, die damit verbunden sein könnten, wenn Du diese Funktionen aktivierst und wenn Du dich damit komfortabel fühlt, kannst du diese aktivieren. Für viele überwiegen die Vorteile des Dateiaustauschs, Bildvorschau und Gruppenchat die möglichen Schäden bei weitem - aber wir verlangen trotzdem von jedem, dass diese Funktionen selber aktiviert werden müssen.

Du kannst die Funktionen jederzeit wieder deaktivieren, alle Funktionen sind lokal innerhalb der Cwtch-App implementiert.


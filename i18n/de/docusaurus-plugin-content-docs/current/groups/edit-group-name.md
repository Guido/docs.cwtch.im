---
sidebar_position: 7
---

# Einen Gruppennamen bearbeiten

:::caution Experimentelle Funktionen erforderlich

Diese Funktion erfordert, dass [Experimente aktiviert](https://docs.cwtch.im/docs/settings/introduction#experiments) und das [Gruppen Experiment](https://docs.cwtch.im/docs/settings/experiments/group-experiment) eingeschaltet ist.

:::

Gruppennamen sind privat, sie werden nicht weitergegeben, sie sind Ihr lokaler Name für die Gruppe.

1. Gehe im Chat-Fenster zu den Einstellungen
2. Ändere den Namen der Gruppe
3. Drücke den Speicherknopf
4. Jetzt hat deine Gruppe einen neuen Namen

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/group_edit.mp4" />
    </video>
</div>
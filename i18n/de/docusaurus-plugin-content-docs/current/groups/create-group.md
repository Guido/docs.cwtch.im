---
sidebar_position: 3
---

# Eine neue Gruppe erstellen

:::caution Experimentelle Funktionen erforderlich

Diese Funktion erfordert, dass [Experimente aktiviert](https://docs.cwtch.im/docs/settings/introduction#experiments) und das [Gruppen Experiment](https://docs.cwtch.im/docs/settings/experiments/group-experiment) eingeschaltet ist.

:::

1. In deinem Kontakt-Bereich
2. Drücke auf die Plus-Schaltfläche
3. Drücke auf Gruppe erstellen
4. Gib deiner Gruppe einen Namen
5. Drücke auf Erstellen

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Group_Create.mp4" />
    </video>
</div>
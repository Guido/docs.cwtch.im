---
sidebar_position: 1
---

# Eine Einführung in die Cwtch Gruppen

:::caution Experimentelle Funktionen erforderlich

Diese Funktion erfordert, dass [Experimente aktiviert](/docs/settings/introduction#experiments) und das [Gruppen Experiment](/docs/settings/experiments/group-experiment) eingeschaltet ist.

:::

**Hinweis: Metadaten resistente Kommunikation ist noch ein aktives Entwicklungsfeld und was hier dokumentiert ist wird sehr wahrscheinlich in der Zukunft sich ändern.**

Standardmäßig unterstützt Cwtch nur Peer-to-Peer online Chats. Um Mehrparteien-Gespräche zu unterstützen und Offline-Auslieferung zu ermöglichen, ist ein (nicht vertrauenswürdiger) Dritter erforderlich. Wir nennen diese Entitäten (Dritten) ["Server"](/docs/servers/introduction)

Diese Server können von jedem eingerichtet werden und sind dafür gedacht, immer online zu sein. Am wichtigsten ist, dass die Kommunikation mit einem Server so gestaltet ist, dass der Server so wenig Informationen wie möglich über den Inhalt oder die Metadaten erfährt.

In vielerlei Hinsicht ist die Kommunikation mit einem Server identisch mit einer regulären Cwtch Peer, alle Schritte werden gleich unternommen, aber der Server fungiert immer als eingehender Peer, und der ausgehende Peer verwendet immer neu generierte **ephemere (flüchtige) Schlüsselpaare** - so dass jede Serversitzung getrennt wird.

Somit unterscheidet sich die Peer zu Server Konversation nur in der *Art* der Nachrichten, die zwischen zwei Seiten gesendet werden, mit dem Server, der alle Nachrichten, die er erhält, speichert und damit den Clients erlaubt den Server nach älteren Nachrichten abzufragen.

Das mit Servern verbundene Risikomodell ist komplizierter als Peer-to-Peer-Kommunikation, da wir derzeit Leute benötigen, die Server in Cwtch mit [opt-in zum Gruppen-Chat-Experiment verwenden möchten](/docs/settings/experiments/group-experiment) um auf nicht vertrauenswürdigen Servern Gruppen hinzuzufügen, zu verwalten und zu erstellen.

## Wie Gruppen unter der Haube funktionieren

Wenn eine Person eine Gruppendiskussion starten will, generiert sie einen zufälligen geheimen `Gruppenschlüssel`. Alle Gruppenkommunikation wird mit diesem Schlüssel verschlüsselt.

Zusammen mit dem `Gruppenschlüssel` entscheidet der Gruppenersteller sich auch für einen **Cwtch Server** als Host der Gruppe zu verwenden. Weitere Informationen darüber, wie Server sich selbst authentifizieren, findest du unter [Schlüsselbündel](https://docs.openprivacy.ca/cwtch-security-handbook/key_bundles.html).

Ein `Gruppen Identifikator` wird mit dem Gruppenschlüssel und dem Gruppenserver generiert und diese drei Elemente sind in einer Einladung verpackt, die an potenzielle Gruppenmitglieder gesendet werden kann (z.B. über existierende Peer-to-Peer-Verbindungen).

Um eine Nachricht an die Gruppe zu senden, verbindet sich ein Profil mit dem Server, der die Gruppe beherbergt (siehe unten), und verschlüsselt deine Nachricht mit dem Gruppenschlüssel `` und erzeugt eine kryptographische Signatur über die `Gruppen-Id`, `Gruppen Server` und die entschlüsselte Nachricht (siehe: [Nachrichtenformate](https://docs.openprivacy.ca/cwtch-security-handbook/message_formats.html) für weitere Informationen).

Um Nachrichten von der Gruppe zu erhalten, verbindet sich ein Profil mit dem Server, der die Gruppe beherbergt und lädt *alle* Nachrichten (seit ihrer vorherigen Verbindung) herunter. Die Profile versuchen dann jede Nachricht mit dem  `Gruppenschlüssel` und wenn dies erfolgreich war, dann die Signatur zu verifizieren (siehe [Cwtch Server](https://docs.openprivacy.ca/cwtch-security-handbook/server.html)  [Cwtch Groupen](https://docs.openprivacy.ca/cwtch-security-handbook/groups.html) für einen Überblick über Attacken und Abschwächungen).



---
sidebar_position: 7
---

# Einen Kontakt blockieren

1. In einem Konversationsfenster
2. Zu Einstellungen gehen
3. Scrolle nach unten zum Kontakt blockieren
4. Verschiebe den Schalter auf Kontakt blockieren

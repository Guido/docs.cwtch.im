---
sidebar_position: 2
---

# Neue Konversationen akzeptieren/ablehnen

1. Zu deinem Profil gehen
2. Wenn dich jemand als Kontakt hinzugefügt hat, dann erscheint ein neuer Name und zwei Optionen
   1. Klicke auf das Herzsymbol um diese neue Verbindung zu akzeptieren
   2. Klicke auf das Papierkorb-Symbol, um sie zu blockieren

Siehe auch: [Einstellung: Blockiere unbekannte Verbindungen](https://docs.cwtch.im/docs/settings/behaviour/block-unknown-connections)
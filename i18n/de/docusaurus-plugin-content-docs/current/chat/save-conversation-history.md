---
sidebar_position: 4
---

# Gesprächsverlauf speichern

Standardmäßig speichert Cwtch aus Gründen der Privatsphäre keine Unterhaltungshistorie zwischen den Sitzungen.

Um den Verlauf für eine bestimmte Konversation zu aktivieren:

1. Gehe in einem Konversationsfenster zu Einstellungen
2. Gehe zu Verlauf speichern
3. Klicke das Dropdown-Menü
4. Verlauf speichern auswählen
5. Jetzt wird dein Verlauf gespeichert

Die Konversationshistorie kann an jedem beliebigen Punkt abgeschaltet werden, indem du im Dropdown-Menü "Verlauf löschen" auswählst.
---
sidebar_position: 1.5
---

# Neuen Kontakt hinzufügen

1. Ein Profil auswählen
2. Klicke auf den Hinzufügen Button
3. Wähle 'Kontakt hinzufügen'
5. Eine Cwtch-Adresse einfügen
6. Der Kontakt wird zu deiner Kontaktliste hinzugefügt
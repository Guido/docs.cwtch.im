---
sidebar_position: 3
---

# Teile eine Cwtch Adresse

1. Zu deinem Profil gehen
2. Klicke auf das Kopiere-Adresse-Symbol

Du kannst diese Adresse nun teilen. Personen mit dieser Adresse können dich als Cwtch Kontakt hinzufügen.

Für Informationen zum Blockieren von Verbindungen von Personen, die du nicht kennst, lies bitte [Einstellungen: Unbekannte Verbindungen blockieren](/docs/settings/behaviour/block-unknown-connections) 
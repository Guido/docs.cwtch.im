---
sidebar_position: 1
---

# Eine Einführung in die Cwtch P2P Chat

Cwtch benutzt Tor v3 Onion Dienste, um anonyme, Peer-to-Peer-Verbindungen zwischen Profilen herzustellen.

## Wie P2P-Chat unter der Haube funktioniert

Um mit deinen Freunden in einem Peer-to-Peer-Gespräch zu chatten, müssen beide online sein.

Nach einer erfolgreichen Verbindung beteiligen sich beide Seiten an einem **-Authentifizierungsprotokoll** von:

* Behauptet, dass jede Seite Zugang zu dem privaten Schlüssel hat, der mit ihrer öffentlichen Identität verbunden ist.
* Erzeugt einen ephemeren Session-Schlüssel, der zur Verschlüsselung aller weiteren Kommunikation während der Sitzung verwendet wird.

Dieser Austausch (detailliert im [-Authentifizierungsprotokoll](https://docs.openprivacy.ca/cwtch-security-handbook/authentication_protocol.html)dokumentiert) ist *offline verweigerbar* d. h. es ist möglich für jede Seite Transkripte dieses Protokoll-Auszutausches zu fälschen und daher ist es unmöglich definitiv zu beweisen, dass der Austausch überhaupt stattgefunden hat.

Sobald der Authentifizierungsprozess erfolgreich ist, kannst Du und dein Freund sicher sein, dass niemand anderes etwas über den Inhalt oder die Metadaten eurer Konversation erfahren kann.

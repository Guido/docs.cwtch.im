---
sidebar_position: 2
---

# Wie man einen Server erstellt

:::caution Experimentelle Funktionen erforderlich

Diese Funktion erfordert, dass [Experimente aktiviert](https://docs.cwtch.im/docs/settings/introduction#experiments) und das [Server Hosting Experiment](https://docs.cwtch.im/docs/settings/experiments/server-hosting) eingeschaltet ist.

:::

1. Zum Server-Symbol gehen
2. Drücke den + Action-Button, um einen neuen Server zu erstellen
3. Wähle einen Namen für deinen Server
4. Wähle ein Passwort für deinen Server
5. Klicke auf "Server hinzufügen"

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Server_New.mp4" />
    </video>
</div>
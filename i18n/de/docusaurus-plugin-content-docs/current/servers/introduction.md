---
sidebar_position: 1
---

# Server Einführung

:::caution Experimentelle Funktionen erforderlich

Diese Funktion erfordert, dass [Experimente aktiviert](https://docs.cwtch.im/docs/settings/introduction#experiments) und das [Server Hosting Experiment](https://docs.cwtch.im/docs/settings/experiments/server-hosting) eingeschaltet ist.

:::

Der Cwtch Kontakt zu Kontakt Chat ist vollständig peer to peer, d. h. wenn ein Teilnehmer offline ist, kannst du mit ihm nicht chatten, und es gibt keinen Mechanismus um mit mehreren Personen zu chatten.

Um Gruppenchat (und Offline-Zustellung) zu unterstützen, haben wir nicht vertrauenswürdige Cwtch-Server erstellt, die Nachrichten für eine Gruppe hosten können. Die Nachrichten werden mit dem Gruppenschlüssel verschlüsselt und über flüchtige Onions abgeholt, so dass der Server keine Möglichkeit hat zu wissen, welche Nachrichten für welche Gruppen er hat oder wer darauf zugreift.

Derzeit werden Server in der Cwtch-App nur auf der Desktop-Version unterstützt, da die Internet-Verbindung von mobilen Geräten zu instabil und nicht geeignet für den Betrieb eines Servers ist.

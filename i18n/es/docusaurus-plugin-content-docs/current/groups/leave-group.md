---
sidebar_position: 6
---

# Cómo abandonar un grupo

:::caution Experimentos Requeridos

Esta función requiere [Experimentos habilitados](https://docs.cwtch.im/docs/settings/introduction#experiments) y el [Experimento de Grupos](https://docs.cwtch.im/docs/settings/experiments/group-experiment) activado.

:::

:::advertencia

Esta función resultará en la eliminación **irreversible** de material clave. Esto **no se puede deshacer**.

:::

1. En el panel de chat ir a la configuración
2. Desplázate hacia abajo en el panel de ajustes
3. Pulsa abandonar la conversación
4. Confirma que quieres salir

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Group_Leave.mp4" />
    </video>
</div>
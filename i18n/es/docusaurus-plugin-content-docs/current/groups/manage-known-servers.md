---
sidebar_position: 7
---

# Administrar servidores

:::caution Experimentos Requeridos

Esta función requiere [Experimentos habilitados](https://docs.cwtch.im/docs/settings/introduction#experiments) y el [Experimento de Grupos](https://docs.cwtch.im/docs/settings/experiments/group-experiment) activado.

:::

Los grupos de Cwtch están alojados en servidores no confiables. Si quieres ver los servidores que conoces, su estado y los grupos alojados en ellos:

1. En el panel de contactos
2. Ve al icono de administrar servidores

## Importar servidor alojado localmente

1. Para importar un servidor alojado localmente haz clic en seleccionar el servidor local
2. Selecciona el servidor que quieras

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Server_Manage.mp4" />
    </video>
</div>
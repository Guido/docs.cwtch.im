---
sidebar_position: 1
---

# Pruebas de Cwtch

Esta sección documenta algunas formas de comenzar con la prueba de Cwtch.

### Ejecutar Fuzzbot

FuzzBot es nuestro bot de pruebas de desarrollo. Puede añadir FuzzBot como contacto: `cwtch:4y2hxlxqzautabituedksnh2ulcgm2coqbure6wvfpg4gi2ci25ta5ad`.

:::info Ayuda de FuzzBot

Enviar a FuzzBot un mensaje de `help` lo activará para enviar una respuesta con todos los comandos de prueba disponibles actualmente.

:::

Para más información sobre FuzzBot consulta nuestro [Blog de desarrollo](https://openprivacy.ca/discreet-log/07-fuzzbot/).

### Únete al Grupo de Pruebas de Candidatos de Lanzamientos de Cwtch

Enviar a Fuzzbot el comando de `testgroup-invite` hará que FuzzBot te invite al **Grupo de Testeadores de Cwtch**! Ahí puedes hacer preguntas, publicar informes de errores y ofrecer comentarios.

### Cwtch Nightlies

Las construcciones de Cwtch Nightly son construcciones de desarrollo que contienen nuevas características que están listas para probar.

Las versiones más recientes de desarrollo de Cwtch están disponibles en nuestro [servidor de compilación](https://build.openprivacy.ca/files/).

Nosotros **no** recomendamos que los testers se actualicen siempre a los últimos Nightlies. En su lugar publicaremos un mensaje en el Grupo de Pruebas de Candidatos de Lanzamientos de Cwtch cuando un Nightlie significativo se encuentre disponible. Un Nightly se considera significativo si contiene una nueva característica o una corrección importante de errores.
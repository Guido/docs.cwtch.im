---
sidebar_position: 2
---

# Traducir Cwtch

Si quieres contribuir a la traducción de Cwtch o de este manual puedes aprender cómo aquí

## Aplicación de Cwtch

La aplicación se traduce a través de [Lokalize](https://lokalise.com).
1. Regístrate para obtener una cuenta en el sitio
2. Envía un correo electrónico a [team@cwtch.im](mailto:team@cwtch.im) diciendo que deseas traducir y la dirección de correo electrónico con la que te registraste. Te agregaremos al proyecto

## Manual del usuario de Cwtch

El manual se traduce a través de [Crowdin](https://crowdin.com).
1. Regístrate en el sitio.
2. Ve al proyecto [cwtch-users-handbook](https://crowdin.com/project/cwtch-users-handbook) y únete.
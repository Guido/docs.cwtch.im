---
sidebar_position: 5
---

# Desbloquear perfiles cifrados

Cuando reinicies Cwtch, si usaste una [contraseña](/docs/profiles/change-password/) para proteger tu perfil, no se cargará por defecto y necesitarás desbloquearlo.

1. Pulsa el icono de desbloqueo rosa <img src="/img/lock_open-24px.webp" className="ui-button" alt="ícono de desbloqueo" />
2. Introduce tu contraseña
3. Haz clic en “desbloquear tu perfil”

Ve también: [Manual de seguridad de Cwtch: Encriptación del perfil & Almacenamiento](https://docs.openprivacy.ca/cwtch-security-handbook/profile_encryption_and_storage.html)

---
sidebar_position: 4
---

# Cambiar tu imagen de perfil

:::caution

Esta función requiere [Experimentos habilitados](https://docs.cwtch.im/docs/settings/introduction#experiments) y tanto  [Archivos Compartidos](https://docs.cwtch.im/docs/settings/experiments/file-sharing) como [Imágenes previas de imagen y fotos](https://docs.cwtch.im/docs/settings/experiments/image-previews-and-profile-pictures) activadas.

:::

1. Pulsa el lápiz junto al perfil que quieres editar
2. Pulsa sobre el lápiz rosa sobre tu foto de perfil
3. Selecciona una imagen de tu dispositivo
4. Desplázate hacia abajo y haz clic en guardar el perfil
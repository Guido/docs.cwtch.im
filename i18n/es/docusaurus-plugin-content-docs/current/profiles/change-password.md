---
sidebar_position: 3
---

# Cambiar tu contraseña

1. Pulsa el lápiz junto al perfil que quieres editar
2. Ve a la contraseña actual e introduce tu contraseña actual
3. Ve a la nueva contraseña e introduce tu nueva contraseña
4. Re introduce tu contraseña
5. Haz clic en Guardar perfil
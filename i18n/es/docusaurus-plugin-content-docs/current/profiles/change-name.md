---
sidebar_position: 2
---

# Cambiar tu nombre para mostrar

1. En la vista Administrar Perfiles, pulsa el lápiz junto al perfil que deseas editar
2. Cambia tu nombre
3. Haz clic en guardar perfil
---
sidebar_position: 10
---

# Copia de seguridad o Exportación de un perfil

En la pantalla de administración de perfiles:

1. Pulsa el lápiz junto al perfil que quieres editar
2. Desplázate hacia abajo hasta la parte inferior de la pantalla
3. Selecciona "Exportar perfil"
4. Elige una ubicación y un nombre de archivo
5. Confirma

Una vez confirmado, Cwtch hará una copia del perfil en la ubicación dada. Este archivo está cifrado al mismo nivel que el perfil. Consulta [Una nota sobre Perfiles protegidos con contraseña (cifrados)](/docs/profiles/create-a-profile#a-note-on-password-protected-encrypted-profiles) para obtener más información sobre perfiles cifrados.

Este archivo puede ser [importado](/docs/profiles/importing-a-profile) en otra instancia de Cwtch en cualquier dispositivo.
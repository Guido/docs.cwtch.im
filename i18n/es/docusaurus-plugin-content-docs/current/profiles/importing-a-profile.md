---
sidebar_position: 11
---

# Importar un perfil

1. Pulsa el botón `+` en la esquina inferior derecha y selecciona "Importar perfil"
2. Selecciona un [archivo de perfil exportado de Cwtch](/docs/profiles/exporting-profile) para importar
3. Introduce la [contraseña](/docs/profiles/create-a-profile#a-note-on-password-protected-encrypted-profiles) asociada con el perfil y confirma.

Una vez confirmado, Cwtch intentará descifrar el archivo proporcionado usando una clave derivada de la contraseña dada. Si es exitoso el perfil aparecerá en la pantalla de Administración de Perfiles y estará listo para usar.

:::note

Aunque un perfil puede ser importado en varios dispositivos, actualmente sólo una versión de un perfil puede estar en uso en todos los dispositivos a la vez.

Los intentos de usar el mismo perfil en varios dispositivos puede resultar en problemas de disponibilidad y fallos de mensajería.

:::
---
sidebar_position: 6
---

# Eliminar un perfil

:::warning

Esta función resultará en la eliminación **irreversible** de material clave. Esto **no se puede deshacer**.

:::

1. Pulsa el lápiz junto al perfil que quieres editar
2. Desplázate hacia abajo hasta la parte inferior de la pantalla
3. Pulsa eliminar
4. Pulsa realmente eliminar perfil
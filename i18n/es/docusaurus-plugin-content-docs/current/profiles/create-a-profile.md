---
sidebar_position: 1.5
---

# Crear un nuevo perfil

1. Pulsa el botón `+` en la esquina inferior derecha y selecciona "Nuevo perfil"
2. Elije un nombre para mostrar
3. Selecciona si deseas proteger este perfil de forma local con cifrado fuerte:
    - Contraseña: tu cuenta está protegida de otras personas que pueden usar este dispositivo
    - Sin contraseña: cualquiera que tenga acceso a este dispositivo puede acceder a este perfil
4. Introduce tu contraseña y vuelve a introducirla
5. Haz clic en añadir un nuevo perfil

## Una nota sobre los perfiles protegidos por contraseña (cifrado)

Los perfiles se almacenan localmente en el disco y se cifran usando una clave derivada de la contraseña conocida por el usuario (a través de pbkdf2).

Ten en cuenta que una vez cifrado, y almacenado en el disco, la única forma de recuperar un perfil es rederivando la contraseña - por lo que no es posible proporcionar una lista completa de perfiles a los que un usuario podría tener acceso hasta que introduzca una contraseña.

Consultar: [Manual de seguridad de Cwtch.: Encriptación del perfil & Almacenamiento](https://docs.openprivacy.ca/cwtch-security-handbook/profile_encryption_and_storage.html)

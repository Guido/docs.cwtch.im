---
sidebar_position: 3
---

# Contenido de notificaciones

1. Ir a la configuración
2. Desplazar a comportamiento
3. El contenido de las notificaciones controla el contenido de las notificaciones
   1. Evento simple: "Nuevo mensaje" solamente
   2. Información de la conversación: "Nuevo mensaje de XXXXX"

---
sidebar_position: 1
---

# Bloquear Conexiones Desconocidas

De forma predeterminada, Cwtch interpreta las conexiones de direcciones Cwtch desconocidas como [Solicitudes de contacto](https://docs.cwtch.im/docs/chat/accept-deny-new-conversation). Puede cambiar este comportamiento a través de la configuración de Bloquear Conexiones Desconocidas.

Si está activado, Cwtch cerrará automáticamente todas las conexiones de las direcciones de Cwtch que no has añadido a tu lista de contactos. Esto evitará que la gente que tenga su dirección cwtch se ponga en contacto contigo a menos que tu también los añadas.

Para habilitar:

1. Ve a la Configuración
2. Habilita Bloquear Contactos Desconocidos
---
sidebar_position: 5
---

# Experimento de Enlaces Cliqueables

:::danger

Esta función, si está activada, presenta un riesgo de **desanonimización**.

**no abras** las URLs de personas en las que no confías. Los enlaces enviados a través de Cwtch se abren a través del navegador **predeterminado** en el sistema. La mayoría de los navegadores web no pueden proporcionar anonimato.

:::

# Activa el Experimento de Enlaces Cliqueables

Los enlaces cliqueables **no** están habilitados por defecto. Para permitir a Cwtch abrir enlaces en mensajes:

1. Ir a Configuración
2. Habilitar Experimentos
3. Activa el `Experimento de Enlaces Cliqueables`

![](/img/clickable_links_experiment.png)

## Riesgos

Los enlaces cliqueables en los mensajes son una característica muy útil, sin embargo, hay riesgos que debes tener en cuenta si decides activar esta función.

Para prevenir la activación accidental, después de hacer clic en un enlace en un mensaje, Cwtch abrirá primero un aviso adicional con dos opciones:

![](/img/clickable_links.png)

1. Copia la URL al portapapeles
2. Abre la URL en el **navegador web predeterminado**

Puedes usar el botón de retroceso de tu dispositivo o hacer clic lejos de este aviso para evitar seleccionar cualquiera de las dos opciones.

**Cwtch No puede protegerte si abres enlaces maliciosos**.

La URL se abre en el **navegador web predeterminado** el cual es probable, como mínimo, que exponga tu dirección IP al servidor que aloja la URL. Las páginas web también pueden usar otras vulnerabilidades del navegador para obtener información adicional, o aprovecharse aún más de tu equipo.

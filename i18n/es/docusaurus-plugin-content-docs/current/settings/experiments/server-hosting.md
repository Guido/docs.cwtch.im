---
sidebar_position: 2
---

# Alojamiento de servidor

**El alojamiento del servidor es actualmente una característica experimental en Cwtch, no está habilitado por defecto.**

1. Ir a Configuración
2. Habilitar Experimentos
3. Habilitar el experimento de Alojamiento de servidor
4. Probablemente también querrás activar el experimento de Grupos si quieres participar en un grupo alojado en tu servidor
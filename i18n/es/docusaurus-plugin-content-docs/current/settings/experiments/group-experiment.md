---
sidebar_position: 1
---

# Habilitar el Experimento de Grupos

1. Ir a Configuración
2. Habilitar Experimentos
3. Habilitar el Experimento de Grupos
---
sidebar_position: 1
---

# Una Introducción a la configuración de Cwtch

## Apariencia

Estos son los ajustes que afectan cómo se ve cwtch, incluyendo temas y localización.

## Comportamiento

Estos ajustes impactan la respuesta de cwtch a ciertos eventos, por ejemplo, notificaciones de nuevos mensajes, o solicitudes de direcciones públicas desconocidas.

## Experimentos

Hay muchas características en Cwtch que a los usuarios les gustaría tener pero cuya implementación requiere metadatos adicionales, o riesgo, más allá del mínimo que Cwtch requiere para operaciones básicas.

Como tal en **Experimentos** encontrarás un número de **ajustes opcionales** que, cuando se activan, proporcionan características adicionales como chat de grupo, intercambio de archivos o formato de mensaje.

Deberías pensar detenidamente al habilitar estas características sobre los nuevos riesgos que podrían implicar, y si estás cómodo optando por esos riesgos. Para muchos, los beneficios de compartir archivos, las previsualizaciones de imágenes y el chat de grupo superan con creces los daños potenciales - pero para otros requerimos que los usuarios opten por usar estas características.

Puede optar por no hacerlo en cualquier momento, todas las características se implementan localmente dentro de la aplicación Cwtch.


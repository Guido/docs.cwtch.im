---
sidebar_position: 1
---

# Cambiar idioma

[Gracias a la ayuda de voluntarios](https://docs.cwtch.im/docs/contribute/translate), la aplicación Cwtch ha sido traducida a muchos idiomas.

Para cambiar el idioma de Cwtch:

1. Abrir configuración
2. La opción superior es "Idioma", puedes usar el menú desplegable para seleccionar el idioma que deseas utilizar.
---
sidebar_position: 4
---

# Modo de streaming/presentación

El modo de streaming / presentación hace la aplicación más visualmente privada. En este modo, Cwtch no mostrará información auxiliar, como las direcciones de Cwtch y otra información confidencial en las pantallas principales.

Esto es útil cuando se toman capturas de pantalla o cuando se muestra Cwtch de una manera más pública.

1. Pulsa el icono de configuración
2. Activar "Modo Streamer"
3. Comprueba que funciona mirando tu perfil o tu lista de contactos
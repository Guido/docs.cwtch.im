---
sidebar_position: 7
---

# Bloquear un contacto

1. En una ventana de conversación
2. Ir a la Configuración
3. Desplázate hacia abajo hasta bloquear contacto
4. Mueve el interruptor para bloquear un contacto

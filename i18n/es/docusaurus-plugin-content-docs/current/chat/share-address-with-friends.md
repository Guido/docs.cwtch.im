---
sidebar_position: 3
---

# Compartir una dirección de Cwtch

1. Ve a tu perfil
2. Haz clic en el icono de copiar dirección

Ahora puedes compartir esta dirección. Las personas con esta dirección podrán añadirte como un contacto de Cwtch.

Para obtener información sobre cómo bloquear conexiones de personas que no conoces, por favor consulta [Configuración: Bloquear Conexiones Desconocidas](/docs/settings/behaviour/block-unknown-connections) 
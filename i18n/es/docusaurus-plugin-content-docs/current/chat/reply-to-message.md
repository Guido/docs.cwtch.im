---
sidebar_position: 5
---

# Contestar a un mensaje

1. Selecciona un mensaje al que deseas responder
2. Desliza un mensaje hacia la derecha
3. Escribe una respuesta al mensaje citado
4. Pulsar enviar
---
sidebar_position: 4.5
---

# Formato de mensajes

:::caution Experiments Required

Esta función requiere [Experimentos habilitados](https://docs.cwtch.im/docs/settings/introduction#experiments) y el [Experimento de Grupos](https://docs.cwtch.im/docs/settings/experiments/message-formatting) activado.

Opcionalmente, puedes habilitar [Enlaces Cliqueables](https://docs.cwtch.im/docs/settings/experiments/clickable-links) para hacer URLs en mensajes cliqueables en Cwtch.

:::

Actualmente Cwtch soporta el siguiente marcado de formato para los mensajes:

* `**negrita**` que renderizará **negrita**
* `*cursiva*` que renderizará *cursiva*
* `código` que renderizará `código`
* `^superscript^` que renderizará <sup>superíndice</sup>
* `_subscript_` que renderizará <sub>subíndice</sub>
* `~~strikthroughh~~` que renderizará <del>strikethrough</del>

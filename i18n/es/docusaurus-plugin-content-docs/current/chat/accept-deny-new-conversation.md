---
sidebar_position: 2
---

# Aceptar/denegar nuevas conversaciones

1. Ir a tu perfil
2. Si alguien te agregó un nuevo nombre y dos opciones aparecerán
   1. Haz clic en el icono de corazón para aceptar esta nueva conexión
   2. Haz clic en el icono de la papelera para bloquear una nueva conexión

Ve también: [Configuración: Bloquear Conexiones Desconocidas](https://docs.cwtch.im/docs/settings/behaviour/block-unknown-connections)
---
sidebar_position: 1.5
---

# Añadir un nuevo contacto

1. Selecciona un perfil
2. Haz clic en el botón Añadir
3. Selecciona 'Agregar contacto'
5. Pega una dirección de Cwtch
6. El contacto se añadirá a tu lista de contactos
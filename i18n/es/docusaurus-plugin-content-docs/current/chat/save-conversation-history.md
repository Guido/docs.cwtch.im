---
sidebar_position: 4
---

# Guardar el historial de conversaciones

Por defecto, por privacidad, Cwtch no conserva el historial de conversaciones entre sesiones.

Para habilitar el historial de una conversación específica:

1. En una ventana de conversación ve a Ajustes
2. Ve a Guardar Historial
3. Pulsa el menú desplegable
4. Elege Guardar el Historial
5. Ahora tu historial se guardará

El historial de conversaciones puede desactivarse en cualquier momento seleccionando "Borrar Historial" en el menú desplegable.
---
sidebar_position: 8
---

# Desbloquear un contacto

1. Selecciona el contacto en tu lista de conversaciónes. Los contactos bloqueados se mueven al final de la lista.
2. Ve a la configuración de conversación
3. Desplázate hacia abajo para bloquear contacto
4. Mueve el interruptor para desbloquear contacto
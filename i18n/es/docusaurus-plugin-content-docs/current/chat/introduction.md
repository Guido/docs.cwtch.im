---
sidebar_position: 1
---

# Una Introducción al Chat P2P de Cwtch

Cwtch usa los servicios Onion de Tor v3 para establecer conexiones anónimas, peer-to-peer entre Perfiles.

## Cómo funciona el chat P2P

Para poder chatear con tus amigos en una conversación peer-to-peer, ambos deben estar en línea.

Después de una conexión exitosa, ambas partes participan en un **protocolo de autenticación** que:

* Verifica que cada parte tenga acceso a la clave privada asociada a su identidad pública.
* Genera una clave de sesión efímera usada para cifrar toda comunicación durante la sesión.

Este intercambio (documentado con más detalle en [protocolo de autenticación](https://docs.openprivacy.ca/cwtch-security-handbook/authentication_protocol.html)) es *negable fuera de línea* i.e. es posible que cualquier parte forje transcripciones de este intercambio de protocolo después del hecho, y como tal - después del hecho - es imposible demostrar definitivamente que el intercambio ha ocurrido en absoluto.

Una vez que el proceso de autenticación haya tenido éxito, tanto tu como tu contacto pueden comunicarse con la certeza de que nadie más puede aprender nada sobre el contenido o los metadatos de su conversación.

---
sidebar_position: 5
---

# Tor

Cwtch utiliza [Tor](https://www.torproject.org/) para proporcionar enrutamiento y conexiones. Usar servicios ocultos de Tor para albergar perfiles y conexiones "efímeras" proporciona fuertes garantías de anonimato a los usuarios de Cwtch a la hora de hacer conexiones.

## Panel de Tor

Dado que estamos añadiendo una capa de red adicional a Cwtch, proporcionamos un panel para ver el estado de la red Tor y hacer cambios. Para acceder a él

1. Desde el panel de lista de perfiles, haz clic en el icono de Tor ![icono de tor](/img/Tor_icon.png)
2. Ver el estado de la red tor

```
Estado de Tor: en línea
Versión Tor: 0.4.6.9
```

### Reiniciar Tor

La red Tor puede ocasionalmente tener conexiones obsoletas que no son detectadas inmediatamente por Tor o por Cwtch (siempre estamos intentando mejorar esto). A veces un usuario puede encontrar contactos o grupos que aparecen fuera de línea que deberían estar en línea. Si quieres reiniciar todas las conexiones de red en Cwtch, proporcionamos un mecanismo para reiniciar tor desde el app. El botón **Reiniciar** reiniciará Tor desde la aplicación de Cwtch.


### Consenso de Caché de Tor

Por defecto iniciamos un proceso nuevo de Tor cada vez que arranca la aplicación, y requiere descargar un estado de red de Tor antes de poder comenzar. Este proceso no es instantáneo. Si quieres acelerar el arranque de Cwtch, puedes habilitar Caché de Conflictos de Tor para acelerar futuros arranques. Si te encuentras con un problema de arranque en el que los datos son obsoletos o corruptos y el reporte de Cwtch no puede arrancar Tor, desactiva esta función y **reinicia tor** de nuevo, y debería funcionar.

### Configuración avanzada de Tor

Ofrecemos la posibilidad de proporcionar avanzadamente la opción de configuración de Tor en esta sección permitiéndote

- Especificar un puerto SOCKS personalizado para conectarse a una conexión Tor existente
- Especifique un puerto de control personalizado para conectarse a una conexión Tor existente
- y especificar opciones adicionales introduciendo opciones `torrc` personalizadas


---
sidebar_position: 6
---

# Cómo desbloquear un servidor

:::caution Experimentos Requeridos

Esta función requiere [Experimentos habilitados](https://docs.cwtch.im/docs/settings/introduction#experiments) y el [Experimento de Grupos](https://docs.cwtch.im/docs/settings/experiments/server-hosting) activado.

:::

Si protegiste tu servidor con una contraseña, tendrá que ser desbloqueado cada vez que reinicies la aplicación.

1. Ve al icono del servidor
2. Pulsa el icono de desbloqueo rosa
3. Introduce la contraseña de tu servidor
4. Pulsa Desbloquear
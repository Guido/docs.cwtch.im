---
sidebar_position: 2
---

# Cómo crear un servidor

:::caution Experimentos Requeridos

Esta función requiere [Experimentos habilitados](https://docs.cwtch.im/docs/settings/introduction#experiments) y el [Experimento de Grupos](https://docs.cwtch.im/docs/settings/experiments/server-hosting) activado.

:::

1. Ve al icono del servidor
2. Pulsa el botón + para crear un nuevo servidor
3. Elige un nombre para tu servidor
4. Selecciona una contraseña para tu servidor
5. Haz clic en "Añadir servidor"

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Server_New.mp4" />
    </video>
</div>
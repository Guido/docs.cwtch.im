---
sidebar_position: 1
---

# Introducción a Servidores

:::caution Experimentos Requeridos

Esta función requiere [Experimentos habilitados](https://docs.cwtch.im/docs/settings/introduction#experiments) y el [Experimento de Grupos](https://docs.cwtch.im/docs/settings/experiments/server-hosting) activado.

:::

El chat de Cwtch entre dos usuarios es completamente par a par (peer-to-peer), lo que significa que si un usuario está desconectado no puedes chatear con él, y no hay un mecanismo para que múltiples personas puedan chatear.

Para dar soporte al chat de grupo (y a la entrega sin conexión) hemos creado servidores no confiables que pueden albergar mensajes para un grupo. Los mensajes se cifran con la clave del grupo, y se obtienen a través de Onions efímeros, por lo que el servidor no tiene forma de saber qué mensajes podría tener para qué grupos, o quién está accediendo a él.

Los servidores que se ejecutan actualmente en la aplicación Cwtc sólo son compatibles con la versión de Escritorio ya que la conexión a Internet de los dispositivos móviles y el entorno móvil es demasiado inestable e inadecuado para ejecutar un servidor.

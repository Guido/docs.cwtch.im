---
sidebar_position: 4
---

# Cómo eliminar un servidor

:::caution Experimentos Requeridos

Esta función requiere [Experimentos habilitados](https://docs.cwtch.im/docs/settings/introduction#experiments) y el [Experimento de Grupos](https://docs.cwtch.im/docs/settings/experiments/server-hosting) activado.

:::


1. Ve al icono del servidor
2. Selecciona el servidor que quieres eliminar
3. Pulsa el icono de lápiz
4. Desplázate hacia abajo e introduce tu contraseña
5. Pulsa borrar
6. Pulsar eliminar realmente
7. Tu servidor ha sido eliminado

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Server_Delete.mp4" />
    </video>
</div>
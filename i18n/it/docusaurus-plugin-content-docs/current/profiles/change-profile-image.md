---
sidebar_position: 4
---

# Modificare la tua immagine del profilo

:::attenzione

Questa funzione richiede [Esperimenti abilitati](https://docs.cwtch.im/docs/settings/introduction#experiments) e sia [Condivisione file](https://docs.cwtch.im/docs/settings/experiments/file-sharing) sia [Anteprime immagine e immagini dei profili](https://docs.cwtch.im/docs/settings/experiments/image-previews-and-profile-pictures) attivate.

:::

1. Clicca sulla matita accanto al profilo che vuoi modificare
2. Clicca sulla matita rosa sopra la foto del tuo profilo
3. Seleziona un'immagine dal tuo dispositivo
4. Scorri verso il basso e clicca sul salva profilo
---
sidebar_position: 11
---

# Importare un profilo

1. Clicca sul pulsante di azione "`+`" nell'angolo in basso a destra e seleziona "Importa profilo"
2. Seleziona il [file di un profilo Cwtch esportato](/docs/profiles/exporting-profile) da importare
3. Inserisci la [password](/docs/profiles/create-a-profile#a-note-on-password-protected-encrypted-profiles) associata al profilo e conferma.

Una volta confermato, Cwtch tenterà di decifrare il file fornito utilizzando una chiave derivata dalla password fornita. Se l'operazione ha successo il profilo verrà visualizzato nella schermata Gestione profilo e sarà pronto per l'uso.

:::nota

Mentre un profilo può essere importato su più dispositivi, attualmente si puó avere una sola versione di un profilo in uso su tutti i dispositivi ad un dato momento.

Tentativi di utilizzare lo stesso profilo su più dispositivi possono causare problemi di disponibilità e errori di messaggistica.

:::
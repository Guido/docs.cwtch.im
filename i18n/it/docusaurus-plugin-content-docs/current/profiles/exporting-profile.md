---
sidebar_position: 10
---

# Backup o esportazione di un profilo

Nella schermata di Gestione del profilo:

1. Seleziona la matita accanto al profilo che vuoi modificare
2. Scorri verso il basso fino alla fine del menú
3. Seleziona "Esporta profilo"
4. Scegli una posizione e un nome per il file
5. Conferma

Una volta confermato, Cwtch inserirà una copia del profilo alla posizione indicata. Questo file viene crittografato allo stesso livello del profilo. Vedi [Una nota sui profili protetti da password (crittografati)](/docs/profiles/create-a-profile#a-note-on-password-protected-encrypted-profiles) per ulteriori informazioni sui profili crittografati.

Questo file può essere [importato](/docs/profiles/importing-a-profile) in un'altra istanza di Cwtch su qualsiasi dispositivo.
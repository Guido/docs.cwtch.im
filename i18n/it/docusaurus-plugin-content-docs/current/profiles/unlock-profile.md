---
sidebar_position: 5
---

# Sbloccare profili crittografati

Quando riavvii Cwtch, se hai usato una [password](/docs/profiles/change-password/) per proteggere il tuo profilo, esso non verrà caricato per impostazione predefinita, e dovrai sbloccarlo.

1. Clicca sull'icona di sblocco rosa <img src="/img/lock_open-24px.webp" className="ui-button" alt="sblocca icona" />
2. Inserisci la tua password
3. Clicca su “Sblocca il tuo profilo”

Vedi anche: [Manuale sulla sicurezza di Cwtch: Crittografia del profilo & spazio di archiviazione](https://docs.openprivacy.ca/cwtch-security-handbook/profile_encryption_and_storage.html)

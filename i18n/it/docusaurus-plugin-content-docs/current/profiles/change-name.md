---
sidebar_position: 2
---

# Cambiare il tuo nome visualizzato

1. Nel menú Gestisci profili, premi la matita accanto al profilo che vuoi modificare
2. Modifica il tuo nome
3. Clicca su salva il profilo
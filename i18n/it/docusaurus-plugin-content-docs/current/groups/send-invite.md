---
sidebar_position: 4
---

# Inviare inviti a un gruppo

:::attenzione Esperimenti necessari

Questa funzione richiede [Esperimenti abilitati](https://docs.cwtch.im/docs/settings/introduction#experiments) e l'[Esperimento Gruppi](https://docs.cwtch.im/docs/settings/experiments/group-experiment) attivato.

:::

1. Vai a una conversazione con un contatto
2. Clicca sull'icona dell'invito
3. Seleziona il gruppo in cui vuoi invitare la persona
4. Seleziona "Invita"
5. Hai inviato un invito

<div>
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Group_Invite.mp4" />
    </video>
</div>
---
sidebar_position: 7
---

# Gestire i server

:::attenzione Esperimenti necessari

Questa funzione richiede [Esperimenti abilitati](https://docs.cwtch.im/docs/settings/introduction#experiments) e l'[Esperimento Gruppi](https://docs.cwtch.im/docs/settings/experiments/group-experiment) attivato.

:::

I gruppi Cwtch sono ospitati da server non affidabili. Se vuoi vedere i server di cui sei a conoscenza, il loro stato e i gruppi da essi ospitati:

1. Nel pannello dei tuoi contatti
2. Vai all'icona di gestione dei server

## Importa un server ospitato localmente

1. Per importare un server ospitato localmente, clicca su "seleziona server locale"
2. Seleziona il server che desideri

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Server_Manage.mp4" />
    </video>
</div>
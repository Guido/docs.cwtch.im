---
sidebar_position: 1
---

# Introduzione ai server

:::attenzione Esperimenti necessari

Questa funzione richiede [Esperimenti abilitati](https://docs.cwtch.im/docs/settings/introduction#experiments) e l' [Esperimento di server hosting](https://docs.cwtch.im/docs/settings/experiments/server-hosting) attivato.

:::

La chat Cwtch da contatto a contatto è completamente peer to peer, il che significa se un peer è offline, non si può chattare, e non c'è alcun meccanismo per chat tra più persone.

Per supportare la chat di gruppo (e il recapito offline) abbiamo creato server Cwtch non affidabili che possono ospitare messaggi per un gruppo. I messaggi sono cifrati con la chiave di gruppo e recuperati tramite servizi "onion" effimeri, in modo che il server non ha modo di sapere di quali messaggi per quali gruppi potrebbe essere in possesso, o chi sta accedendo.

Attualmente fare girare dei server nell'app di Cwtch è supportato solo nella versione Desktop in quanto i dispositivi di telefonia mobile hanno connessione internet e configurazione troppo instabili o inadatte ad ospitare un server.

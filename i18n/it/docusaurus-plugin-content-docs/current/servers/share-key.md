---
sidebar_position: 5
---

# Come condividere il tuo pacchetto di chiavi del server

:::attenzione Esperimenti necessari

Questa funzione richiede [Esperimenti abilitati](https://docs.cwtch.im/docs/settings/introduction#experiments) e l' [Esperimento di server hosting](https://docs.cwtch.im/docs/settings/experiments/server-hosting) attivato.

:::

Il pacchetto di chiavi del server è il pacchetto di dati di cui un'app Cwtch ha bisogno per poter usare un server. Se vuoi solo far conoscere agli altri utenti Cwtch il tuo server, puoi condividere questo pacchetto con loro. Chi riceve il pacchetto avrà a quel punto la capacità di creare i propri gruppi sul server.

1. Vai all'icona del server
2. Seleziona il server desiderato
3. Utilizza l'icona di "copia indirizzo" per copiare le chiavi del server
4. Non condividere le chiavi con persone di cui non ti fidi

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Server_Keys.mp4" />
    </video>
</div>
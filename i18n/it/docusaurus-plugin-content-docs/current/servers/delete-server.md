---
sidebar_position: 4
---

# Come eliminare un server

:::attenzione Esperimenti necessari

Questa funzione richiede [Esperimenti abilitati](https://docs.cwtch.im/docs/settings/introduction#experiments) e l' [Esperimento di server hosting](https://docs.cwtch.im/docs/settings/experiments/server-hosting) attivato.

:::


1. Vai all'icona del server
2. Seleziona il server che vuoi eliminare
3. Clicca sull'icona della matita
4. Scorri verso il basso e inserisci la tua password
5. Clicca su elimina
6. Clicca su elimina definitivamente
7. Il tuo server è stato eliminato

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Server_Delete.mp4" />
    </video>
</div>
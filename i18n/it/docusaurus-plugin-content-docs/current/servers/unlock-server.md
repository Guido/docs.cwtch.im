---
sidebar_position: 6
---

# Come sbloccare un server

:::attenzione Esperimenti necessari

Questa funzione richiede [Esperimenti abilitati](https://docs.cwtch.im/docs/settings/introduction#experiments) e l' [Esperimento di server hosting](https://docs.cwtch.im/docs/settings/experiments/server-hosting) attivato.

:::

Se hai protetto il tuo server con una password, dovrà essere sbloccato ogni volta che riavvii l'applicazione.

1. Vai all'icona del server
2. Clicca sull'icona di sblocco rosa
3. Inserisci la password del tuo server
4. Premi Sblocca
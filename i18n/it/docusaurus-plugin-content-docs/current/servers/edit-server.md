---
sidebar_position: 3
---

# Come modificare un server

:::attenzione Esperimenti necessari

Questa funzione richiede [Esperimenti abilitati](https://docs.cwtch.im/docs/settings/introduction#experiments) e l' [Esperimento di server hosting](https://docs.cwtch.im/docs/settings/experiments/server-hosting) attivato.

:::

1. Vai all'icona del server
2. Seleziona il server che vuoi modificare
3. Clicca sull'icona della matita
4. Cambia la descrizione / abilita o disabilita il server
5. Clicca su "Salva server"

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/server_edit.mp4" />
    </video>
</div>
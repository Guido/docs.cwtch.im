---
sidebar_position: 1.5
---

# Aggiungere un nuovo contatto

1. Seleziona un profilo
2. Clicca sul pulsante "Aggiungi"
3. Scegli "Aggiungi contatto"
5. Incolla un indirizzo Cwtch
6. Il contatto verrà aggiunto alla tua lista contatti
---
sidebar_position: 7
---

# Bloccare un contatto

1. Dalla finestra di una conversazione
2. Vai su "Impostazioni"
3. Scorri verso il basso fino a "Blocca il contatto"
4. Sposta l'interruttore su "Blocca contatto"

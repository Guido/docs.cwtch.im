---
sidebar_position: 1
---

# Un'introduzione alla chat p2p di Cwtch

Cwtch utilizza i servizi onion di Tor v3 per stabilire connessioni anonime, peer-to-peer tra profili.

## Come funziona la chat p2p internamente

Per interagire con una persona tra i tuoi contatti in una conversazione peer-to-peer entrambi dovete essere online.

Dopo che la connessione ha successo, entrambe le parti sono coinvolte in un **protocollo di autenticazione** che:

* Si assicura che ogni parte abbia accesso alla chiave privata associata alla propria identità pubblica.
* Genera una chiave di sessione effimera utilizzata per cifrare tutte le comunicazioni successive durante la sessione.

Questo scambio (documentato in dettaglio nel [protocollo di autenticazione](https://docs.openprivacy.ca/cwtch-security-handbook/authentication_protocol.html)) è *negabile offline*, ovvero è possibile per qualsiasi parte forgiare trascrizioni di questo protocollo di scambio a posteriori, e di conseguenza - a fatto avvenuto - è impossibile dimostrare definitivamente che lo scambio è avvenuto davvero.

Una volta che il processo di autenticazione è riuscito, potete comunicare con l'altra persona indisturbatamente, con la sicurezza che nessun altro può apprendere nulla sui contenuti o i metadati della conversazione.

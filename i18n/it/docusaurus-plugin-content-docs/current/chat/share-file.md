---
sidebar_position: 6
---

# Condividere un file

:::attenzione Esperimenti necessari

Questa funzione richiede [Esperimenti abilitati](https://docs.cwtch.im/docs/settings/introduction#experiments) e l'[Esperimento di condivisione file](https://docs.cwtch.im/docs/settings/experiments/file-sharing) attivato.

Come opzione ulteriore, puoi abilitare [Anteprime immagine e Immagini del profilo](https://docs.cwtch.im/docs/settings/experiments/image-previews-and-profile-pictures) per visualizzare le anteprime delle immagini condivise nella finestra di conversazione.

:::

In una conversazione,

1. Clicca sull'icona dell'allegato
2. Trova il file che vuoi inviare
3. Conferma che vuoi inviarlo

## Come funziona la condivisione file con i gruppi? I miei file sono salvati su un server da qualche parte?

I file vengono inviati tramite le connessioni Cwtch onion-to-onion direttamente dalla persona che offre il file alla persona che lo riceve. L'offerta iniziale per inviare un file è pubblicata come una conversazione standard in Cwtch / un normale messaggio in sovraimpressione. Per i gruppi, ciò significa che l'offerta iniziale (contenente il nome del file, la dimensione, l'hash e un nonce) è pubblicata sul server del gruppo, ma poi ogni destinatario si connette al mittente individualmente per ricevere effettivamente il contenuto del file.

## Questo significa che devo essere online per inviare un file?

Si. Se la persona che offre il file va offline, dovrai aspettare che sia di nuovo online per riprendere il trasferimento del file. Il protocollo sottostante divide i file in pezzi verificabili e che possono essere richiesti individualmente, in modo tale che in un futura versione sarà possibile "riospitare" un file pubblicato in un gruppo, e anche scaricare da più host contemporaneamente (come una sorta di bittorrent).

## Perché ci sono dei nuovi contatti che appaiono nella mia lista?

Ciò è dovuto al modo in cui Cwtch gestisce attualmente le connessioni da indirizzi sconosciuti. Dato che pubblicare un file in un gruppo implica che i membri del gruppo si connettono a te direttamente, alcuni di questi membri potrebbero non essere già nella tua lista contatti, e quindi la loro connessione a te per il download apparirà nella tua lista come una richiesta di contatto.

## Che cosa è "SHA512"?

SHA512 è un [hash crittografico](https://en.wikipedia.org/wiki/Cryptographic_hash_function) che può essere utilizzato per verificare che il file scaricato sia una copia corretta del file offerto. Cwtch fa questa verifica per te automaticamente, ma puoi anche provare a fare la tua propria verifica indipendentemente! Nota che includiamo anche un nonce casuale con le offerte di file, cosicché le persone non possano semplicemente richiederti qualsiasi hash casuale che potresti avere, o file di conversazioni di cui non fanno parte.

## C'è un limite alla dimensione dei file?

Il limite attuale è di 10 gigabyte per file.

## Che cosa sono questi file .manifest?

I file .manifest vengono utilizzati durante il download di un file per verificare che i singoli pezzi siano ricevuti correttamente, e supportare il ripristino di trasferimenti interrotti. Essi contengono anche le informazioni provenienti dall'offerta del file originale. Puoi eliminarli senza problemi una volta completato il download. Su Android, i manifesti vengono memorizzati nella cache dell'app e possono essere cancellati attraverso le impostazioni di sistema.

## E i metadati dei file?

Inviamo il nome del file come suggerimento e per aiutare a distinguerlo dalle offerte di altri file. Il percorso completo viene rimosso prima di inviare l'offerta. Bisogna fare attenzione ai metadati nascosti che potrebbero essere memorizzati nel file stesso, cosa che varia a seconda del formato del file. Ad esempio, le immagini potrebbero contenere informazioni sulla geo-localizzazione e informazioni sulla fotocamera che le ha catturate, e i file PDF sono noti per contenere informazioni nascoste come il nome dell'autore o il dispositivo su cui sono stati creati. In generale, si dovrebbe inviare/ricevere file solo a/da persone di cui ci si fida.

## Posso scaricare file automaticamente?

Se l'esperimento [Anteprima immagini e immagini del profilo](https://docs.cwtch.im/docs/settings/experiments/image-previews-and-profile-pictures) è abilitato, Cwtch scaricherà automaticamente le immagini dalle conversazioni accettate

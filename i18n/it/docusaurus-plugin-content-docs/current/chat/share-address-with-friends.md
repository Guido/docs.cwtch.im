---
sidebar_position: 3
---

# Condividere un indirizzo Cwtch

1. Vai al tuo profilo
2. Fare clic sull'icona della copia indirizzo

Ora è possibile condividere questo indirizzo. Le persone con questo indirizzo saranno in grado di aggiungerti come un contatto Cwtch.

Per informazioni su come bloccare connessioni da persone che non conosci vedi [Impostazioni: Blocca connessioni sconosciute](/docs/settings/behaviour/block-unknown-connections) 
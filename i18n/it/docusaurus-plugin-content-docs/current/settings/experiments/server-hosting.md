---
sidebar_position: 2
---

# Hosting di un server

**L'hosting di un server è attualmente una funzione sperimentale in Cwtch, non è tra le impostazioni predefinite.**

1. Vai su "Impostazioni"
2. Abilita "Esperimenti"
3. Abilita l'Esperimento di server hosting
4. Probabilmente vorrai anche abilitare l'Esperimento Gruppi se vuoi partecipare a un gruppo ospitato sul tuo server
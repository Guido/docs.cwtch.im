---
sidebar_position: 1
---

# Abilita l'Esperimento Gruppi

1. Vai su "Impostazioni"
2. Abilita "Esperimenti"
3. Abilita l'Esperimento Gruppi
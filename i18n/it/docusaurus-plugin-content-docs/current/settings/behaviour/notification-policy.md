---
sidebar_position: 2
---

# Policy di notifica

1. Vai alle impostazioni
2. Scorri fino a "comportamento"
3. La policy di notifica controlla il comportamento delle notifiche
4. Clicca su "Predefinita" per modificare il comportamento per attivare o per disabilitare tutte le notifiche
5. Scegli la tua policy di notifica preferita
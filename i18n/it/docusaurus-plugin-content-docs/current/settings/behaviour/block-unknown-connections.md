---
sidebar_position: 1
---

# Blocca connessioni sconosciute

Come impostazione predefinita, Cwtch interpreta le connessioni da indirizzi cwtch sconosciuti come [Richieste di contatto](https://docs.cwtch.im/docs/chat/accept-deny-new-conversation). È possibile modificare questo comportamento tramite l'impostazione "Blocco connessioni sconosciute".

Se abilitata, Cwtch chiuderà automaticamente tutte le connessioni dagli indirizzi Cwtch che non hai aggiunto al tuo elenco delle conversazioni. Questo impedirà alle persone che hanno il tuo indirizzo cwtch di contattarti a meno che tu non li aggiunga prima.

Per abilitare:

1. Vai su "Impostazioni"
2. Attiva/Disattiva "Blocco contatti sconosciuti"
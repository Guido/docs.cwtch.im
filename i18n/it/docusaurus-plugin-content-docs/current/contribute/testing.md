---
sidebar_position: 1
---

# Testare Cwtch

Questa sezione documenta alcuni modi per iniziare a testare Cwtch.

### Far girare Fuzzbot

FuzzBot è il nostro bot di test di sviluppo. Puoi aggiungere FuzzBot come contatto: `cwtch:4y2hxlxqzautabituedksnh2ulcgm2coqbure6wvfpg4gi2ci25ta5ad`.

:::Info Aiuto FuzzBot

L'invio a FuzzBot del messaggio `aiuto` lo attiverà ad inviare una risposta con tutti i comandi di test attualmente disponibili.

:::

Per maggiori informazioni su FuzzBot consulta il nostro [blog sullo sviluppo Discreet Log](https://openprivacy.ca/discreet-log/07-fuzzbot/).

### Unisciti al gruppo dei tester delle versioni di Cwtch candidate alla pubblicazione ufficiale

L'invio a Fuzzbot del comando `testgroup-invite` farà sì che FuzzBot ti inviti al **gruppo di tester di Cwtch**! Lì puoi fare domande, inviare segnalazioni di bug e offrire feedback.

### Cwtch Nightlies

Cwtch Nightly build sono build di sviluppo che contengono nuove funzionalità che sono pronte per essere testate.

Le versioni di sviluppo più recenti di Cwtch sono disponibili dal nostro [build server](https://build.openprivacy.ca/files/).

**Non** raccomandiamo che i tester si tengano sempre aggiornati con l'ultimo nightly build. Invece, pubblicheremo un messaggio sul gruppo Tester di versioni di Cwtch candidate al rilascio quando sarà disponibile un nightly build significativa. Un nightly build è considerato significativo se contiene una nuova funzione o un fix a un bug serio.
---
sidebar_position: 2
---

# Traduci Cwtch

Se vuoi contribuire con delle traduzioni all'applicazione di Cwtch o a questo manuale, ecco come fare

## Applicazione di Cwtch

L'applicazione è tradotta attraverso [Lokalise](https://lokalise.com).
1. Registra un account sul sito
2. Manda una email a [team@cwtch.im](mailto:team@cwtch.im) dicendo che vorresti tradurre e specifica l'indirizzo email con cui hai effettuato la registrazione a Lokalise. Ti aggiungeremo al progetto.

## Manuale utente di Cwtch

Il manuale è tradotto attraverso [Crowdin](https://crowdin.com).
1. Registra un account sul sito.
2. Vai al progetto [cwtch-users-handbook](https://crowdin.com/project/cwtch-users-handbook) e unisciti.
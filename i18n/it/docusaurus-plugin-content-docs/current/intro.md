---
sidebar_position: 1
---

# Che cos’è Cwtch?

Cwtch (/kʊtʃ/ - una parola gallese che si traduce approssimativamente in “un abbraccio che crea un luogo sicuro”) è un'applicazione di messaggistica decentralizzata, rispettosa della privacy e resistente ai metadati.

* **Decentralizzata e aperta**: non c'è alcun “servizio Cwtch” o “rete Cwtch”. I partecipanti a Cwtch possono ospitare i propri spazi sicuri, o prestare la loro infrastruttura ad altri che cercano uno spazio sicuro. Il protocollo Cwtch è aperto, e chiunque è libero di creare bot, servizi e interfacce utente, e di integrare e interagire con Cwtch.
* **Rispettosa della privacy**: Tutta la comunicazione in Cwtch è crittografata end-to-end e si svolge attraverso i servizi onion di Tor v3.
* **Resistente ai metadati**: Cwtch è stata progettata in modo tale che nessuna informazione venga scambiata o resa disponibile a qualcuno senza il suo esplicito consenso, inclusi i messaggi on-the-wire e i metadati del protocollo.


# Sicurezza e Crittografia

Per uno sguardo più approfondito alla sicurezza, alla privacy e alla sottostante tecnologia di crittografia utilizzata in Cwtch, consulta il nostro [Manuale sulla sicurezza](https://docs.openprivacy.ca/cwtch-security-handbook/)

# Come iniziare

Puoi scaricare l'ultima versione di Cwtch da [https://cwtch.im/download/](https://cwtch.im/download/)


---
sidebar_position: 5
---

# Tor

Cwtch utilizza [Tor](https://www.torproject.org/) per routing e connessioni. L'utilizzo dei servizi nascosti di Tor per ospitare profili e connessioni "effimere" generate al volo durante la creazione di una connessione fornisce forti garanzie di anonimato agli utenti di Cwtch.

## Pannello Tor

Dato che stiamo aggiungendo un ulteriore livello di rete a Cwtch, forniamo un pannello per visualizzare lo stato della rete Tor e apportare modifiche. Per accedervi

1. dal pannello Elenco profili, fare clic sull'icona Tor ![icona di tor](/img/Tor_icon.png)
2. Visualizza lo stato della rete tor

```
Stato Tor: Online
Versione Tor: 0.4.6.9
```

### Resetta Tor

La rete Tor stessa può occasionalmente avere connessioni obsolete che non vengono rilevate immediatamente da essa o da Cwtch (cerchiamo continuamente di introdurre miglioramenti a riguardo). A volte un utente può trovare contatti o gruppi visualizzati offline che ritiene dovrebbero invece essere online. Se vuoi riavviare tutte le connessioni di rete in Cwtch, forniamo un meccanismo per riavviare tor dall'interno dell'app. Il pulsante **reset** riavvierà Tor dall'interno dell'applicazione Cwtch.


### Abilita la cache del Consenso Tor

Come impostazione predefinita, avviamo un nuovo processo Tor ogni volta che l'app si avvia, e ció richiede il download di un qualche stato di rete Tor prima che possa iniziare. Questo processo non è istantaneo. Se vuoi velocizzare gli avvii futuri di Cwtch, puoi abilitare la memorizzazione nella cache del Consenso Tor. Se riscontri un problema di avvio in cui i dati sono obsoleti o danneggiati e Cwtch segnala che non è possibile avviare Tor, disabilita questa funzione e **resetta** tor nuovamente, e dovrebbe funzionare.

### Configurazione avanzata di Tor

Offriamo anche la possibilità di fornire opzioni di configurazione Tor avanzate in questa sezione consentendoti di

- specificare una porta SOCKS personalizzata per connettersi a un processo Tor esistente
- specificare una porta Controllo personalizzata per connettersi a un processo Tor esistente
- specificare ulteriori opzioni inserendo `torrc` opzioni personalizzate


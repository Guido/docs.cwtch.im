# Website

This website is built using [Docusaurus 2](https://docusaurus.io/), a modern static website generator.

### Installation

```
$ yarn
```

or 

install [NVM](https://github.com/nvm-sh/nvm) to manage multiple and new NodeJS versions, and then install NodeJS 16

```
$ nvm install 16
$ nvm use 16
```

*nvm only works in bash shell*

### Local Development

```
$ yarn start
```

or

```
$ npm run start
```

This command starts a local development server and opens up a browser window. Most changes are reflected live without having to restart the server.

### Build

```
$ yarn build
```

or

```
$ npm run build
```

This command generates static content into the `build` directory and can be served using any static contents hosting service.

### Deployment

Using git.openprivacy.ca to manage but deployment by push is to a seperate docs server. Add your key to the docusaurus account's .ssh/authorized_keys and then

```
$ git remote add docs docusaurus@docs.cwtch.im:/home/docusaurus/git/docs.cwtch.im
$ git push docs main
```

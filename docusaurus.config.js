// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'The Cwtch Handbook',
  tagline: 'Your Guide to setting up, and using, Surveillance Resistant Infrastructure',
  url: 'https://docs.cwtch.im',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.png',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'Open Privacy Research Society', // Usually your GitHub org/user name.
  projectName: 'cwtch.im', // Usually your repo name.

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en', 'es', 'de', 'it']
    // Planned future versions to add. removing to start cus each needs a compile on push, no point compiling untranslated dups
    //, 'fr', 'da', 'de', 'it', 'no', 'pl', 'ro', 'ru', 'es-ES', 'cy'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://git.openprivacy.ca/cwtch.im/docs.cwtch.im/src/branch/main/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      image: 'img/cwtch_handbook_header.jpg',
      colorMode: {
	            defaultMode: 'dark',
      },
      navbar: {
        title: 'Cwtch Handbook',
        logo: {
          alt: 'Cwtch Logo',
          src: 'img/knott.png'
        },
        items: [
          {
            type: 'doc',
            docId: 'intro',
            position: 'left',
            label: 'Cwtch Intro',
          },
          {
            href: 'https://git.openprivacy.ca/cwtch.im',
            label: 'Git',
            position: 'right',
          },
          {
            type: 'localeDropdown',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'Tutorial',
                to: '/docs/intro',
              },
            ],
          },
          {
            title: 'Community',
            items: [
              {
                label: 'Twitter',
                href: 'https://twitter.com/cwtch_im',
              },
            ],
          },
          {
            title: 'More',
            items: [
              {
                label: 'Home',
                href: 'https://cwtch.im',
              },
              {
                label: 'Git',
                href: 'https://git.openprivacy.ca/cwtch.im',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Open Privacy Research Society.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
    /*plugins: [
    [
      require.resolve("@cmfcmf/docusaurus-search-local"),
      {
        // Options here
        language: ["en", 'fr', 'da', 'de', 'it', 'no', 'ro', 'ru', 'es'],
      },
    ],
   ],*/

};

module.exports = config;

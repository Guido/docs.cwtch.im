---
sidebar_position: 4
---

# Streamer/Presentation Mode

Streamer/Presentation mode makes the app more visually private. In this mode, Cwtch will not display
auxiliary information like cwtch addresses and other sensitive information on the main screens.

This is useful when taking screenshots or otherwise displaying Cwtch in a more public way.

1. Press the settings icon
2. Toggle "Streamer Mode" to On
3. Check it works by looking at your profile or at your contact list
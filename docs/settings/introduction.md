---
sidebar_position: 1
---

# An Introduction to Cwtch App Settings

## Appearance 

These are settings which effect how cwtch looks, including themes and localization. 

## Behaviour 

These settings impact how cwtch responds to certain events e.g. notifications for new messages, or requests
from unknown public addresses.

## Experiments

There are many features in Cwtch that people would like to have but whose implementation requires additional metadata, or
risk, beyond the minimum that Cwtch requires for basic operations.

As such under **Experiments** you will find a number of **optional** settings which, when enables, provide
additional features like group chat, file sharing or message formatting.

You should think carefully when enabling these features about the new risks that might be involved, and if you
are comfortable opting-in to those risks. For many the benefits of file sharing, image previews and group chat
far outweigh the potential harms - but for other we require everyone to opt-in to these features.

You can opt-out at any time, all features are implemented locally within the Cwtch app.


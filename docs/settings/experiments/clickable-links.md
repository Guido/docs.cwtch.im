---
sidebar_position: 5
---

# Clickable Links Experiment

:::danger

This feature, if enabled, presents a **deanonymization** risk.

Do **not** open URLs from people you do not trust. Links sent via Cwtch are opened via the **default**
browser on the system. Most web browsers cannot provide anonymity.

:::

# Enable the Clickable Links Experiment

Clickable links are **not** enabled by default. To allow Cwtch to open links in messages:

1. Go to Settings
2. Enable Experiments
3. Enable the `Clickable Links` Experiment

![](/img/clickable_links_experiment.png)

## Risks

Clickable links in messages are a very useful feature however there are risks you should be aware of if you
choose to enable this feature. 

To prevent accidental triggering, after clicking on a link in a message, Cwtch will first open an additional prompt with two options:

![](/img/clickable_links.png)

1. Copy the URL to the Clipboard
2. Open the URL in the **default web browser**

You can use the back button on your device, or click away from this prompt to avoid selecting either option.

**Cwtch cannot protect you if you open malicious links**. 

The URL is opened in the **default web browser** which will likely, at a minimum, expose your IP address to the server hosting the URL. Web pages may also use other browser vulnerabilities to gather additional information, or further exploit your computer.

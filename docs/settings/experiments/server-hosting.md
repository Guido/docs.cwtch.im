---
sidebar_position: 2
---

# Server Hosting

**Server hosting is currently an experimental feature in Cwtch, it is not enabled by default.**

1. Go to Settings
2. Enable Experiments
3. Enable the Server Hosting experiment
4. You will probably also want to enable the Group experiment if you want to participate on a group hosted on your server
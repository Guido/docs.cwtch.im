---
sidebar_position: 1
---

# Enable the Groups Experiment

1. Go to Settings
2. Enable Experiments
3. Enable the Group experiment
---
sidebar_position: 5
---

# Tor

Cwtch uses [Tor](https://www.torproject.org/) to provide routing and connections. Using Tor hidden services to host profiles and on the fly generated "ephemeral" connections when making a connection provides strong anonymity guarantees to users of Cwtch.

## Tor Pane

Since we are adding an additional networking layer to Cwtch, we provide a pane to view Tor network status and make changes. To access it

1. From the profile list pane, click the Tor icon ![tor icon](/img/Tor_icon.png)
2. View the tor network status

```
Tor Status: Online
Tor Version: 0.4.6.9
```

### Reset Tor

The Tor network itself can occasionally have stale connections that aren't detected immediatly by it or Cwtch (we're always trying to improve this). Sometimes a user may find contacts or groups appearing offline they feel should be online. If you'd like to restart all the networking connections in Cwtch, we provide a mechanism to reboot tor from within the app. The **reset** button will reboot Tor from within the Cwtch app.


### Cache Tor Consensus

By default we start a fresh Tor process every time the app boots, and it requires downloading some Tor network state before it can start. This process is not instant. If you want to speed up Cwtch booting, you can enable Caching Tor Conensus to speed up future boots. If you run into a boot problem where the data is stale or corrupted and Cwtch is reporting it cannot boot Tor, disable this feature and **reset** tor again, and it should work.

### Advanced Tor Configuration

We also offer the option to provide advance Tor configuration option in this section by allowing you to

- Specify a custom SOCKS port to connect to an existing Tor over
- Specify a custom Control port to connect to an existing Tor over
- and specify further options by entering custom `torrc` options


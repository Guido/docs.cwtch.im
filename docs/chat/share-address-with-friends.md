---
sidebar_position: 3
---

# Sharing a Cwtch Address

1. Go to your profile
2. Click the copy address icon

You can now share this address. People with this address will be able to add you as a Cwtch contact.

For information on blocking connections from people you don't know please see [Settings: Block Unknown Connections](/docs/settings/behaviour/block-unknown-connections) 
---
sidebar_position: 1.5
---

# Adding a New Contact

1. Select a Profile
2. Click on the Add button
3. Chose 'Add Contact'
5. Paste a Cwtch Address
6. The contact will be added to your contacts list
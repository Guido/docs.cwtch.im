---
sidebar_position: 8
---

# Unblocking a Contact

1. Select the contact in your Conversation list. Blocked contacts are moved to the bottom of the list.
2. Go to Conversation Settings
3. Scroll down to Block Contact
4. Move the switch to Unblock Contact
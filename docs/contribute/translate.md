---
sidebar_position: 2
---

# Translate Cwtch

If you would like to contribute translations to Cwtch the application or this handbook here is how

## Cwtch Application

The application is translated through [Lokalise](https://lokalise.com). 
1. Sign up for an account on the site
2. Email [team@cwtch.im](mailto:team@cwtch.im) that would like to translate and the email address you signed up with. We will add you to the project

## Cwtch User's Handbook

The handbook is translated through [Crowdin](https://crowdin.com).
1. Sign up for an account on the site.
2. Go to the [cwtch-users-handbook](https://crowdin.com/project/cwtch-users-handbook) project and join.
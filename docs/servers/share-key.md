---
sidebar_position: 5
---

# How to share your Server Key Bundle

:::caution Experiments Required

This feature requires [Experiments Enabled](https://docs.cwtch.im/docs/settings/introduction#experiments) and
the [Server Hosting Experiment](https://docs.cwtch.im/docs/settings/experiments/server-hosting) turned on.

:::

Your server key bundle is the package of data a Cwtch app needs in order to use a server. If you just want to make other Cwtch users aware of your server, you can share this with them. Then they will have the ability to create their own groups on the server.

1. Go to the server Icon
2. Select the server you want
3. Use the copy address icon to copy the server keys
4. Don’t share the keys with people you don’t trust

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Server_Keys.mp4" />
    </video>
</div>
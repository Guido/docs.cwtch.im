---
sidebar_position: 4
---

# How to delete server

:::caution Experiments Required

This feature requires [Experiments Enabled](https://docs.cwtch.im/docs/settings/introduction#experiments) and
the [Server Hosting Experiment](https://docs.cwtch.im/docs/settings/experiments/server-hosting) turned on.

:::


1. Go to the server Icon
2. Select the server you want to delete
3. Press the pencil icon
4. Scroll down and input your password
5. Press delete
6. Press really delete
7. Your server is deleted

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Server_Delete.mp4" />
    </video>
</div>
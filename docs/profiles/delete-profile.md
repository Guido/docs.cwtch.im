---
sidebar_position: 6
---

# Deleting a Profile

:::warning

This feature will result in **irreversible** deletion of key material. This **cannot be undone**.

:::

1. Press the pencil next to the profile you want to edit
2. Scroll down to the bottom of the screen
3. Press delete
4. Press really delete profile
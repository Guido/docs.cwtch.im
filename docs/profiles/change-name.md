---
sidebar_position: 2
---

# Changing Your Display Name

1. On the Manage Profiles view, Press the pencil next to the profile you want to edit
2. Change your name
3. Click save profile
---
sidebar_position: 1.5
---

# Creating a New Profile

1. Press the `+` action button in the right bottom corner and select "New Profile"
2. Select a display name
3. Select if you want to protect this profile locally with strong encryption:
    - Password: your account is protected from other people who may use this device
    - No Password: anyone who has access to this device may be able to access this profile
4. Fill in your password and re-enter it
5. Click add new profile

## A note on Password Protected (Encrypted) Profiles

Profiles are stored locally on disk and encrypted using a key derived from user-known password (via pbkdf2).

Note that once encrypted, and stored on disk, the only way to recover a profile is by rederiving the password - as such it isn't possible to provide a full list of profiles a user might have access to until they enter a password.

See also: [Cwtch Security Handbook: Profile Encryption & Storage](https://docs.openprivacy.ca/cwtch-security-handbook/profile_encryption_and_storage.html)
